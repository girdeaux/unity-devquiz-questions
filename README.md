### I've made some sanity(?) assumptions that should probably be obvious from my answers, but I've added some extra comments here and there.

# Q1:
- Does it try to be a class or a function? It returns a value, but modifies its own properties
- Continuing on that, `this` doesn't refer to anything (meaningful/intended at least, function context unclear), it's just a function in a global scope. And the function implies returning something: now it modifies (tries) something outside of it.
- `edibilityChecked`? If any of the checks become invalid in some time, it's still marked as edible, even though it's not.
- WHAT is edible? (again no clear context on what it tries to be). No props are initialized in the example, at least.
- BONUS: `inspectorId` is not-null checked, but no actual checks are done based on it (unless it implies "an inspector with ID x verified it already)
- Yeah that was more than two.

# Q2:
- `delete` is different for some reason, should be `DELETE /users/:id/delete` like all the others
- `Error: Username already exists"` is returned, but statuscode is 200 (OK) for some reason
- HTTP status code should reflect the error (like `409: Conflict`)
- Replace `-` with `_` in `update-timezone`. Likely to end up with technical debt.

# Q3:
- Same salt for all users (if one is compromised, rest are as well)
- Salt stays the same: should be updated when the password changes
- Can change password without confirmation (like previous pw), depending on how User is exposed
- Critical methods are not private

# Q4:
- Use i++ for clear increments
- Cache `deposits.length` for some speed (JS-engines at least) (might be marginal nowadays..)
- Needlessly verbose for a simple operation, would use `reduce()`: `deposits.reduce((prev, current) => prev + current)`
- Breaking `);` after `for`
 
# Q5:
- No null/type checks
- No check if `amt` is even a positive number (could be abused to transfer negative credits)
- Check if `amt` is zero when your at it to prevent needless operations

# Q6:
- Method is named `increaseBalance` but allows decrease also. Quite misleading.
- No null/type/existence checks for any params checks
- No check if amount is a valid number (and positive)
- Could make it more generic by using it as `updateBalance(amount, isCredit)` if we don't want separate methods.

# Q7
- Caching (most requested queries etc.)
- Batch operations
- Specific queries (don't fetch what you don't need, get what you with JOINs etc.)
- Profile on what actually is the bottleneck, and improve on that

# Q8 and Q9
See here:
- https://gitlab.com/girdeaux/unity-devquiz-statscollector
- https://gitlab.com/girdeaux/unity-devquiz-async
